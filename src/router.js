import Vue from "vue";
import Router from "vue-router";
import EventList from "./components/EventList.vue";
import About from "./components/About.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "events",
      component: EventList
    },
    {
      path: "/about",
      name: "about",
      component: About
    }
  ]
});
