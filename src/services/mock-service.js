import faker from "faker";

export class Event {
  constructor({
    id,
    title,
    description,
    image,
    from,
    to,
    latitude,
    longitude
  }) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.image = image;
    this.from = from;
    this.to = to;
    this.latitude = latitude;
    this.longitude = longitude;
  }
}

const events = [];
for (let i = 0; i < 100; i++) {
  events.push(
    new Event({
      id: faker.random.uuid(),
      title: faker.random.words(5),
      description: faker.random.words(100),
      image: `https://picsum.photos/400/600?i=${i}`,
      from: faker.date.past(1),
      to: faker.date.future(1),
      latitude: faker.address.latitude(),
      longitude: faker.address.longitude()
    })
  );
}

export function getEvents() {
  return Promise.resolve(events);
}

export function getEvent(id) {
  return events.find(event => event.id === id);
}

export default {
  Event,
  getEvents,
  getEvent
};
